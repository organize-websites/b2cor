import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'homescreen.dart';
import 'splashscreen.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'B2Cor App',
      home: const SplashPage(),
      routes: <String, WidgetBuilder>{
        '/HomePage': (BuildContext context) =>  const HomePage(title: '',)
      },
    );
  }
}
