class Usuario2 {
  late String url; 
  Usuario2({required this.url});
  Usuario2.fromJson(Map<String, dynamic> json) {
    url = json['url'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['url'] = url;
    return data;
  }
  @override
  String toString() {
    return 'Usuario(token: $url)';
  }
}